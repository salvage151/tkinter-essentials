#Liam Wilson
from tkinter import *

window = Tk()
window.title("Move the Ball")

x1 = 200
x2 = 300
y1 = 200
y2 = 300

emptyCanvas= Canvas(window, width = 500, height = 500, bg = 'white')
emptyCanvas.create_oval(x1,y1,x2,y2, fill = "purple", tags = "ball")
frame = Frame(window)
def ballDown():
	global y1
	global y2
	y1 += 25
	y2 += 25
	emptyCanvas.delete("ball")
	emptyCanvas.create_oval(x1,y1,x2,y2, fill = "purple", tags = "ball")
def ballUp():
	global y1
	global y2
	y1 -= 25
	y2 -= 25
	emptyCanvas.delete("ball")
	emptyCanvas.create_oval(x1,y1,x2,y2, fill = "purple", tags = "ball")
def ballLeft():
	global x1
	global x2
	x1 -= 25
	x2 -= 25
	emptyCanvas.delete("ball")
	emptyCanvas.create_oval(x1,y1,x2,y2, fill = "purple", tags = "ball")
def ballRight():
	global x1
	global x2
	x1 += 25
	x2 += 25
	emptyCanvas.delete("ball")
	emptyCanvas.create_oval(x1,y1,x2,y2, fill = "purple", tags = "ball")
bUp = Button(frame, text = 'Up', command = ballUp)
bDown = Button(frame, text = 'Down', command = ballDown)
bLeft = Button(frame, text = 'Left', command = ballLeft)
bRight = Button(frame, text = 'Right', command = ballRight)
bUp.grid(row = 1, column = 1)
bDown.grid(row = 1, column = 2)
bLeft.grid(row = 1, column = 3)
bRight.grid(row = 1, column = 4)
frame.pack()
emptyCanvas.pack()
window.mainloop()
