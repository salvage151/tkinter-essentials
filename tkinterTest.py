from tkinter import *

class InvestCalc:
	def __init__(self):
		window = Tk()
		window.title("Investment Calculator")

		Label(window, text = "Starting Amount").grid(row = 1, column = 1, sticky = S)
		Label(window, text = "Investment Interest Rate").grid(row = 2, column = 1, sticky = S)
		Label(window, text = "Number of Years").grid(row = 3, column = 1, sticky = S)
		Label(window, text = "End Result").grid(row = 4, column = 1, sticky = s)

		self.annualInterestRateVar = StringVar()
		Entry(window, textvariable = self.annualInterestRateVar, justify = Center).grid(row = 1, column = 2)
		self.numberofYearsVar = StringVar()
		Entry(window, textvariable = self.numberofYearsVar, justify = Center).grid(row = 2, column = 2)
		self.startingInvestmentVar = StringVar()
		Entry(window, textvariable = self.startingInvestmentVar, justify = Center).grid(row = 3, column = 2)
		self.endResultVar = StringVar()
		Label(window, textvaribale = self.endResultVar).grid(row = 4, column = 2)
		Button(window, text = "Calculate Investment", command = self.calcResult).grid(row = 4, column = 2, sticky = s)

		window.mainloop()


InvestCalc()