#Liam Wilson

from tkinter import *

class InvestCalc:
	def calcResult(self):
		monthlyInterest = float(self.annualInterestRateVar.get()) / 1200
		f = float(self.startingInvestmentVar.get()) * \
			(1 + monthlyInterest) ** (float(self.numberofYearsVar.get())*12)
		self.endResultVar.set('{0:10.2}'.format(f))
	def __init__(self):
		window = Tk()
		window.title("Investment Calculator")

		Label(window, text = "Starting Amount").grid(row = 1, column = 1, sticky = S)
		Label(window, text = "Investment Interest Rate").grid(row = 2, column = 1, sticky = S)
		Label(window, text = "Number of Years").grid(row = 3, column = 1, sticky = S)
		Label(window, text = "End Result").grid(row = 4, column = 1, sticky = S)

		self.annualInterestRateVar = StringVar()
		Entry(window, textvariable = self.annualInterestRateVar, justify = CENTER).grid(row = 1, column = 2)
		self.numberofYearsVar = StringVar()
		Entry(window, textvariable = self.numberofYearsVar, justify = CENTER).grid(row = 2, column = 2)
		self.startingInvestmentVar = StringVar()
		Entry(window, textvariable = self.startingInvestmentVar, justify = CENTER).grid(row = 3, column = 2)
		self.endResultVar = StringVar()
		Label(window, textvariable = self.endResultVar, justify = CENTER).grid(row = 4, column = 2)
		Button(window, text = "Calculate Investment", command = self.calcResult).grid(row = 5, column = 2, sticky = S)

		window.mainloop()

InvestCalc()